<?php


Route::get('/', 'WelcomeController@show');
Route::get('/welcome', 'WelcomeController@show');



Route::get('/register', 'RegistrationController@register');
Route::post('/register', 'RegistrationController@postRegister');

Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@postLogin');
// Route::post('/login', 'LoginController@postLogin');


Route::post('/logout', 'LoginController@logout');

Route::get('/user_room', 'VideoController@show');
Route::post('/add_video', 'VideoController@add_video');
Route::get('/user_room', 'VideoController@get_video');

Route::get('/video_player', 'VideoPlayer@show');
Route::post('/add_comment', 'VideoPlayer@add_comment');
//Route::post('/add_comment', 'VideoPlayer@add_comment_ajax');
// 
//Route::get('/video_player', 'VideoPlayer@get_comment');

