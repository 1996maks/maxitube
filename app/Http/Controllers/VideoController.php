<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Auth;
use Sentinel;
use App\Videos;
use Storage;
use Illuminate\Support\Facades\Input;
use FFMpeg;


class VideoController extends Controller
{
    public function show(){
        return view('user_room');
    }

    public function add_video(Request $request){

        
        if(Input::hasFile('video_file')){
            $video = Input::file('video_file');
            $filename = $video->getClientOriginalName();
            $video->move('uploads/videos', $filename, file_get_contents($request->file('video_file')->getRealPath()));


            $output_video = "uploads/videos/$filename";
            $image = "images/$filename.jpg";
            shell_exec("ffmpeg -i $output_video -deinterlace -an -ss 1 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $image 2>&1");
            
}

        $Videos = new Videos;
        $Videos->user_id = Sentinel::getUser()->id;
        
        $Videos->added_by = Sentinel::getUser()->first_name;

        $Videos->video_name = $request->video_name;
        $Videos->path = $filename;
        $videorolik = $Videos->path;
        $Videos->screenshot_path = $image;
        $Videos->save();

        return redirect('user_room');

    }

    public function get_video(){
        
        //$videos = DB::table('videos')->get();   
        $videos = DB::select('select * from videos where user_id =:user_id',['user_id' => Sentinel::getUser()->id]);
        return view('user_room', compact('videos'));

    }
}
