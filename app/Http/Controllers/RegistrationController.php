<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;

class RegistrationController extends Controller
{
    public function register(){
        return view('authentication.register');
    }

    public function postRegister(Request $request){

    //      if(Sentinel::check()){
    //     $user = Sentinel::registerAndActivate($request->all());
    //     return redirect('/');

    //    }else { 
    //        return redirect('/register'); 
    //        } 

        
        $user = Sentinel::registerAndActivate($request->all());
        
        return redirect('/');
        
    }
}
