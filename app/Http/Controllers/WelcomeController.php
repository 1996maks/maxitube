<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Auth;
use Sentinel;
use App\Videos;
use Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use App\User;

class WelcomeController extends Controller
{
    public function show(){
        

        // $videos = DB::select('
        //     SELECT
        //         videos.*,
        //         COALESCE((    
        //             SELECT AVG(raitings.raiting)
        //             FROM raitings
        //             WHERE
        //                 raitings.video_id = videos.id
        //         ), 0) as avg_raiting
        //     FROM
        //         videos
        //     ORDER BY avg_raiting DESC;
        // ');

        $videos = DB::table('videos')
            ->leftJoin('raitings', 'raitings.video_id', '=', 'videos.id')
            ->groupBy('videos.id')
            ->orderByRaw('SUM(raitings.raiting)/COUNT(raitings.id) DESC')
            ->select(['videos.*', DB::raw('AVG(raitings.raiting) as avg_raiting')])
            ->paginate(6);

       
        
        return view('welcome', [
            'videos' => $videos
        ]);
    
        
    }
}
