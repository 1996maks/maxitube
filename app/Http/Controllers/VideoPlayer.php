<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Videos;
use Illuminate\Support\Facades\DB; 
use Sentinel;
use App\Raitings;


class VideoPlayer extends Controller
{
    public function show(){
        // getting video id
        $video_id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        
        // selecting single video record from database
        $video = DB::select('select * from videos where id='.$video_id.' ');
        $video = count($video) == 0 ? false : $video[0];//select the first video        
        if (!$video) {
            return redirect('user_room');
        }

        // Getting all comments from database, where video_id = selected video id
        $raitings = DB::select('select * from raitings where video_id='.$video_id.' ORDER BY raiting DESC ');
        
        $average_raiting = DB::select('select AVG(raiting) from raitings where video_id='.$video_id.' ');
        $average_raiting = count($average_raiting) == 0 ? false : $average_raiting[0];
        $average_raiting = (array)$average_raiting;

        //print_r($average_raiting);
        

        return view('video_player', [
            'video' => $video,
            'raitings' => $raitings,
            'average_raiting' => $average_raiting
        ]);
    }

    public function add_comment(Request $request){

        
        if (Sentinel::check())
{       

        $video_id = $request->video_id;
        $Raitings = new Raitings;
        $Raitings->video_id = $video_id;
        
        $Raitings->user_id = Sentinel::getUser()->id;
        $Raitings->added_by = Sentinel::getUser()->first_name;
        $Raitings->comment = $request->commentar;
        $Raitings->raiting = $request->raiting;
        $Raitings->save();

        return redirect('video_player?id='.$video_id);

}
else
{
        return redirect('login');
}
    

         
        
    }

}





 // public function add_comment_ajax(Request $request){

        
    //     $video_id = $request->video_id;
    //     $Raitings = new Raitings;
    //     $Raitings->video_id = $video_id;
        
    //     $Raitings->user_id = Sentinel::getUser()->id;
    //     $Raitings->added_by = Sentinel::getUser()->first_name;
    //     $Raitings->comment = $request->comment;
    //     $Raitings->raiting = $request->raiting;
    //     $Raitings->save();


    //     // $video_id = isset($_GET['id']) ? intval($_GET['id']) : 0;
        
    //     // selecting single video record from database
    //     $video = DB::select('select * from videos where id='.$video_id.' ');
    //     $video = count($video) == 0 ? false : $video[0];//select the first video        
    //     if (!$video) {
    //         return redirect('user_room');
    //     }

    //     // Getting all comments from database, where video_id = selected video id
    //     $raitings = DB::select('select * from raitings where video_id='.$video_id.' ORDER BY raiting DESC ');
        
    //     $average_raiting = DB::select('select AVG(raiting) from raitings where video_id='.$video_id.' ');
    //     $average_raiting = count($average_raiting) == 0 ? false : $average_raiting[0];
    //     $average_raiting = (array)$average_raiting;

    //     //print_r($average_raiting);
        



    //     return json_encode([
    //         'status'=>'ok',
    //         //'video' => $video,
    //         'raitings' => $raitings,
    //         'average_raiting' => $average_raiting
    //     ]);

    // }