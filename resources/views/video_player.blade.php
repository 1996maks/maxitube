
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" id="meta_csrf_token"/>
    <title>Video Player</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://getbootstrap.com/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/star-rating.css">
    <!--link rel="stylesheet" href="css/images/loading.gif"-->
    <link rel="stylesheet" href="css/lightslider.css">
    <link rel="stylesheet" href="css/images/slider/controls.png">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/lightslider.js"></script>
    <script src="js/star-rating.js"></script>
    <script src="js/script.js"></script>


   <style>
   body{
     background: #66CDAA;
     overflow-x:hidden;
     
   }
   .comment{
     background-color: #F0E68C;
     width: 80%;
     padding:10px 25px;
     border-radius:25%;
     margin-bottom:10px;
   }
   .video_player{
     text-align:center;
     padding: 0px;
     margin-top: 0px;
   }
   .videoWrapper {

	padding-bottom: 30%; /* 16:9 */
	padding-top: 25px;
	height: 0;
   margin:0 auto;
  text-align:center;

}

   </style>
   
  </head>

  <body>

    <div class="container">
     
     @include('layouts.top-menu')
     
     @yield('content')
     
     </div> 

     <div class="videoWrapper">
    <!-- Copy & Pasted from YouTube -->
    <iframe width="560" height="349" src="uploads/videos/<?=$video->path?>" frameborder="0" allowfullscreen></iframe>
    <br>
    <label><?=$video->video_name?></label>
    <br>
    <label>Added by: <?=$video->added_by?></label>
    <?php
    
foreach($average_raiting as $value)
{
  $average = $value;
  echo "<br>";
  echo "<input id='input-id' name='raiting' type='number' class='rating' disabled=disabled value = $average>";
  echo "<br>";
  echo "<br>";
  
}?>
    </div>

<?php
     foreach($raitings as $value){
      ?>
        <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">

    <?php
         echo '<br>';
         echo '<br>';
        echo '<br>';
         echo "<div class='comment' id='comment'>";
         echo "<h3>$value->comment</h3>"; 
         echo '</div>';
         echo "<input id='input-id' name='raiting' type='number' data-size='xs' class='rating' disabled=disabled value = $value->raiting>";
         echo "<h4>Added_by: $value->added_by</h4>";
         echo "<h5>Created at: $value->created_at</h4>";
         
         
          ?>
        </div>
  </div>
 <?php
     }
    ?>
      <br>
      <br>
      <br>
      
      <form action="add_comment" method="post" id="add_comment" class="form-horizontal">
      <input type="hidden" name="video_id" id= "video_id" value="<?= $video->id ?>"  />
  <?php
     echo '<br>';
    ?>
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
    <input id="input-id" name="raiting" type="number" id="stars_raiting" class="rating" min=0 max=5 step=0.5 >
    </div>
  </div>
  
   <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-8">
    
      <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
      <input type="text" name="commentar" class="form-control" id="commentar" placeholder="Enter Comment" required>
    </div>
  </div>
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Add Comment</button>
      
    </div>
  </div>
</form>


  </body>
</html>