 <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            @if(Sentinel::check())
            <li role="presentation">
                <form action="/logout" method="post" id="logout-form">
                {{csrf_field()}}

                <a href="#" onclick="document.getElementById('logout-form').submit()">Logout</a>
                </form>
            </li>
            @else
            <li role="presentation" class="active"><a href="/welcome">Home</a></li>
            <li role="presentation"><a href="/login">Login</a></li>
            <li role="presentation"><a href="/register">Register</a></li>
            @endif

            
          </ul>
        </nav>
        @if(Sentinel::check())
        <h3 class="text-muted"><a href="/welcome">MaxiTube</a></h3>

        <h3>Hello,<a href="/user_room"> {{Sentinel::getUser()->first_name}}</a></h3>
        @else
        <h3 class="text-muted">MaxiTube</h3>
        @endif
      </div>