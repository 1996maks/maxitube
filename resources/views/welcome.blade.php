<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Welcome</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link href="http://getbootstrap.com/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    

    <link rel="stylesheet" href="css/app.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="css/star-rating.css">
    <script src="js/star-rating.js"></script>
    <!--script src="js/script.js"></script-->


  </head>
  <style>
  body{
     background: url(css/images/background/2.jpg) 100% 100% no-repeat;
     background-size: cover;
    
   }
  .data{
    background:white;
    margin-left:40px;
    font-size:20px;
    margin-bottom:25px;

    width:340px;
    height: 250px;
  }
  </style>
  <body>

    <div class="container">
     
     @include('layouts.top-menu')
     
     @yield('content')
     
     </div>   

     <div>
<?php
     foreach($videos as $value){
        ?>
    <div class="col-md-4 content" >
     <a href="video_player?id=<?=$value->id?>"><img src="<?php echo $value->screenshot_path; ?>" style = "width:340px;">
       <?php
         echo "<div class = 'data';>";
         echo '<br>';
         echo "<input id='input-id' name='raiting' type='number' data-size='xs' class='rating' disabled=disabled value = $value->avg_raiting>";
         
         echo "<p>$value->video_name</p>";         
         echo "<h4>Added by: $value->added_by</h4>";
         echo '<br>';
         echo '</div>';
        
         
          ?>
          
       </a>   
       </div>  

<?php
     }
     
    ?>
  <div class="container text-center">
 <?php echo $videos->render();?>
</div>

<?php
     echo '<br>';
    ?>
   
  </body>
</html>
