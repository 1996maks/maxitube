
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>User Room</title>

    <!-- Bootstrap core CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">

    <link href="http://getbootstrap.com/examples/jumbotron-narrow/jumbotron-narrow.css" rel="stylesheet">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/lightslider.css">
    <link rel="stylesheet" href="css/images/slider/controls.png">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/lightslider.js"></script>
    <script src="js/script.js"></script>
   
  </head>
  <style>
  body{
     background: url(css/images/background/1.jpg) 100% 100% no-repeat;
     background-size: cover;
   }
   .info{
     color: #F4A460;
     font-weight: bold;
   }
  </style>
  <body>

    <div class="container">
     
     @include('layouts.top-menu')
     
     @yield('content')
     
     </div> 
     
<ul id="lightSlider">
<?php
     foreach($videos as $value){     
       
        ?>

  <li>
     <a href="video_player?id=<?=$value->id?>"><img src="<?php echo $value->screenshot_path; ?>" style = "width:340px;">
       <?php

         //echo $value->screenshot_path;
         echo '<br>';
         echo "<h3 class='info'>$value->video_name</h3>";
        
         //echo $value->added_date;
         
         echo "<h4 class='info'>Added by: $value->added_by</h4>";
         echo '<br>';

         
          ?>
          
       </a>     
  </li>
  
<?php
     }
  
    ?>
</ul>
<?php
     echo '<br>';
    ?>


 <form action="add_video" method="post" class="form-horizontal" enctype="multipart/form-data">
  <div class="form-group">
    <label class="control-label col-sm-2" >Video Name:</label>
    <div class="col-sm-6">
    
      <input type="text" name="video_name" class="form-control" id="video_name" placeholder="Enter Name" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" >File:</label>
    <div class="col-sm-6"> 
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
      <input type="file" name="video_file" class="form-control" id="file" required>
    </div>
  </div>
  
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Add Video</button>
      
    </div>
  </div>
</form>


  </body>
</html>



















<!--
<ul id="lightSlider">
<1?php

     foreach($videos as $value){
       
        ?>
  <li>
     <a href="video_player?id=<1?=$value->id?>"><img src="<1?php echo $value->screenshot_path; ?>" style = "width:340px;">
       <1?php

         //echo $value->screenshot_path;
         echo '<br>';
         echo "<h3>$value->video_name</h3>";
        
         //echo $value->added_date;
         
         echo "<h4>Added by: $value->added_by</h4>";
         echo '<br>';

         
          ?>
          
       </a>     
  </li>
<1?php
     }
  
    ?>
 
</ul>


<1?php
     echo '<br>';
    ?>

 <form action="add_video" method="post" class="form-horizontal" enctype="multipart/form-data">
  <div class="form-group">
    <label class="control-label col-sm-2" >Video Name:</label>
    <div class="col-sm-6">
    
      <input type="text" name="video_name" class="form-control" id="video_name" placeholder="Enter Name" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2" >File:</label>
    <div class="col-sm-6"> 
    <input type="hidden" name="_token" value="{{{ csrf_token() }}}">
      <input type="file" name="video_file" class="form-control" id="file" required>
    </div>
  </div>
  
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Add Video</button>
      
    </div>
  </div>
</form>


  </body>
</html>-->

 
 

 